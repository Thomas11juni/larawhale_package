@extends('lw::layout.default')

@section('content')

    <h1>Pages</h1>

    <?php

        $model = new LaraWhale\App\Models\Page();

        $columns = $model->get_columns();

    ?>

    <div class="clearfix">

        <a class="pull-right btn btn-default" href="{{ route('pages.create') }}">

            Create

        </a>

    </div>

    {!! view('lw::module.table', [
        'columns' => $columns,
        'items' => $pages,
        'resource' => 'pages'
    ])->render() !!}

@endsection
