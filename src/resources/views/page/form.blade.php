@extends('lw::layout.default')

@section('content')

    <?php

        # Check model
        #
        $page = (isset($page) ? $page : NULL);


        # Form parameters
        #
        $route_name = request()->route()->getName();

        $route = ($route_name == 'pages.create' ? route('pages.store') : route('pages.update', $page));

        $method = ($route_name == 'pages.create' ? 'POST' : 'PUT');

    ?>

    {!! Form::open([
        'class' => 'col-xs-12',
        'files' => true,
        'method' => $method,
        'url' => $route
    ]) !!}

        <div class="form-group">

            {!! Form::label('title', 'Title') !!}

            {!! Form::text('title', $page ? $page->title : NULL, [
                'class' => 'form-control'
            ]) !!}

            {!! view('lw::module.error_label', ['key' => 'title'])->render() !!}

        </div>

        <div class="form-group">

            {!! Form::label('uri', 'Uri') !!}

            {!! Form::text('uri', $page ? $page->route->uri : NULL, [
                'class' => 'form-control'
            ]) !!}

            {!! view('lw::module.error_label', ['key' => 'uri'])->render() !!}

        </div>

        <div class="form-group">

            {!! Form::label('view', 'View') !!}

            {!! Form::text('view', $page ? $page->view : NULL, [
                'class' => 'form-control'
            ]) !!}

            {!! view('lw::module.error_label', ['key' => 'view'])->render() !!}

        </div>

        @if ($route_name == 'pages.edit')

            <hr>

            {!! view('lw::module.sections_form', [
                'view' => $page->view,
                'owner' => $page
            ])->render() !!}

        @endif

        <div class="form-group">

            {!! Form::submit('submit', [
                'class' => 'btn btn-default'
            ]) !!}

        </div>

    {!! Form::close() !!}

@endsection
