@extends('lw::layout.default')

@section('content')

    <?php
        # Check model
        #
        $list = (isset($list) ? $list : NULL);


        # Form parameters
        #
        $route_name = request()->route()->getName();

        $route = ($route_name == 'lists.create' ? route('lists.store') : route('lists.update', $list));

        $method = ($route_name == 'lists.create' ? 'POST' : 'PUT');
    ?>

    {!! Form::open([
        'class' => 'col-xs-12',
        'files' => true,
        'method' => $method,
        'url' => $route
    ]) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title') !!}

            {!! Form::text('title', $list ? $list->title : NULL, [
                'class' => 'form-control'
            ]) !!}

            {!! view('lw::module.error_label', ['key' => 'title'])->render() !!}
        </div>

        <div class="form-group">
            {!! Form::label('model', 'Model') !!}

            {!! Form::text('model', $list ? $list->model : NULL, [
                'class' => 'form-control'
            ]) !!}

            {!! view('lw::module.error_label', ['key' => 'model'])->render() !!}
        </div>

        <div class="form-group">
            {!! Form::submit('submit', [
                'class' => 'btn btn-default'
            ]) !!}
        </div>

    {!! Form::close() !!}

@endsection
