@extends('lw::layout.default')

@section('content')

    <h1>List: {{ $list->title }}</h1>

    <h2>List items</h2>

    <?php
        $model = new $list->model();

        $columns = get_columns($model);
    ?>

    <div class="clearfix">
        <a class="pull-right btn btn-default" href="{{ route('list_items.create', $list->id) }}">
            Create
        </a>
    </div>

    {!! view('lw::module.table', [
        'columns' => $columns,
        'delete_route' => function ($list_item) use ($list) {
            return route('list_items.delete', [
                'list_id' => $list->id,
                'list_item_id' => $list_item->id
            ]);
        },
        'edit_route' => function ($list_item) use ($list) {
            return route('list_items.edit', [
                'list_id' => $list->id,
                'list_item_id' => $list_item->id
            ]);
        },
        'items' => $list->list_items,
        'resource' => 'list_items'
    ])->render() !!}

@endsection
