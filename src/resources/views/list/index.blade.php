@extends('lw::layout.default')

@section('content')

    <h1>Lists</h1>

    <?php

        $model = new LaraWhale\App\Models\LwList();

        $columns = $model->get_columns();

    ?>

    <div class="clearfix">

        <a class="pull-right btn btn-default" href="{{ route('lists.create') }}">

            Create

        </a>

    </div>

    {!! view('lw::module.table', [
        'columns' => $columns,
        'items' => $lists,
        'resource' => 'lists'
    ])->render() !!}

@endsection
