<?php

    if (!array_key_exists($lwField['section'], $sections)) return;

    $fields = $sections[$lwField['section']];


    if (!$fields->keys()->contains($lwField['key'])) return;

    $field = $fields[$lwField['key']];


    switch ($lwField['type'])
    {
        case 'file':

            echo $field->file->full_url;

        break;
        
        default:

            echo $field->value;

        break;
    }

?>
