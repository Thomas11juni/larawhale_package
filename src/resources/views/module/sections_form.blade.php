<div class="row">

    <?php $sections = ViewHelper::get_view_sections($view); ?>

    @foreach($sections as $section)

        <?php $owner_section = $owner->sections()->where('key', $section['key'])->first(); ?>

        <div class="col-xs-12">

            <h3>{{ ucfirst($section['key']) }}</h3>

            @foreach ($section['fields'] as $field)

                <div class="form-group">

                    <?php

                        $relation = $field['type'] . '_fields';

                        $section_field = $owner_section ? $owner_section->$relation()->where('key', $field['key'])->first() : NULL;

                        $input_name = 'sections['.$section['key'].']['.$field['key'].']';

                    ?>

                    {!! Form::label($field['key'], ucfirst($field['key'])) !!}

                    {!! Form::hidden($input_name.'[key]', $field['key']); !!}

                    {!! Form::hidden($input_name.'[type]', $field['type']); !!}

                    @if ($field['type'] == 'textarea')

                        {!! Form::textarea($input_name.'[value]', $section_field ? $section_field->value : NULL, [
                            'class' => 'form-control'
                        ]) !!}

                    @elseif ($field['type'] == 'file')

                        {!! Form::file($input_name.'[value]', [
                            'class' => 'form-control'
                        ]) !!}

                    @else

                        {!! Form::input($field['type'], $input_name.'[value]', $section_field ? $section_field->value : NULL, [
                            'class' => 'form-control'
                        ]) !!}

                    @endif

                </div>

            @endforeach

        </div>

    @endforeach

</div>
