
<table class="table">
    <thead>
        <tr>
            @foreach($columns as $column)

                <th>{{ $column }}</th>

            @endforeach
        </tr>
    </thead>

    <tbody>

        @foreach($items as $item)

            <tr>
                @foreach($columns as $column)
                    <td>{{ $item->$column }}</td>
                @endforeach

                <td>
                    <?php
                        $show_url = '';

                        if (isset($show_route))
                        {
                            $show_url = !is_callable($show_route) ?: $show_route($item);
                        }
                        else
                        {
                            $show_url = route($resource.'.show', $item);
                        }
                    ?>

                    <a href="{{ $show_url }}" class="btn btn-xs btn-success">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>

                    <?php
                        $edit_url = '';

                        if (isset($edit_route))
                        {
                            $edit_url = !is_callable($edit_route) ?: $edit_route($item);
                        }
                        else
                        {
                            $edit_url = route($resource.'.edit', $item);
                        }
                    ?>

                    <a href="{{ $edit_url }}" class="btn btn-xs btn-primary">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <?php
                        $delete_url = '';

                        if (isset($delete_route))
                        {
                            $delete_url = !is_callable($delete_route) ?: $delete_route($item);
                        }
                        else
                        {
                            $delete_url = route($resource.'.destroy', $item);
                        }
                    ?>

                    {!! Form::open([
                        'class' => 'action-confirm',
                        'method' => 'DELETE',
                        'url' => $delete_url,
                        'style' => 'display: inline-block;'
                    ]) !!}

                        <button class="btn btn-xs btn-danger">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach

    </tbody>
</table>
