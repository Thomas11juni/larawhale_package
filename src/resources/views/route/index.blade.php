@extends('lw::layout.default')

@section('content')

    <h1>Routes</h1>

    <?php

        $model = new LaraWhale\App\Models\Route();

        $columns = $model->get_columns();

    ?>

    <div class="clearfix">

        <a class="pull-right btn btn-default" href="{{ route('routes.create') }}">

            Create

        </a>

    </div>

    {!! view('lw::module.table', [
        'columns' => $columns,
        'items' => $routes,
        'resource' => 'routes'
    ])->render() !!}

@endsection
