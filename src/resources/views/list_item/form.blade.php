@extends('lw::layout.default')

@section('content')

    <?php
        # Check model
        #
        $list_item = (isset($list_item) ? $list_item : NULL);


        # Form parameters
        #
        $route_name = request()->route()->getName();

        $route = ($route_name == 'list_items.create' ? route('list_items.store') : route('list_items.update', ['list_id' => $list->id, 'list_item_id' => $list_item->id]));

        $method = ($route_name == 'list_items.create' ? 'POST' : 'PUT');
    ?>

    {!! Form::open([
        'class' => 'col-xs-12',
        'files' => true,
        'method' => $method,
        'url' => $route
    ]) !!}

        {!! Form::hidden('lw_list_id', $list->id) !!}

        @foreach ($list_item->fields as $key => $type)

            <div class="form-group">

                {!! Form::label($key, ucfirst($key)) !!}

                @if ($type == 'textarea')

                    {!! Form::textarea($key, $list_item ? $list_item->$key : NULL, [
                        'class' => 'form-control'
                    ]) !!}

                @elseif ($type == 'file')

                    {!! Form::file($key, [
                        'class' => 'form-control'
                    ]) !!}

                @else

                    {!! Form::input($type, $key, $list_item ? $list_item->$key : NULL, [
                        'class' => 'form-control'
                    ]) !!}

                @endif

            </div>

        @endforeach


        <div class="form-group">
            {!! Form::submit('submit', [
                'class' => 'btn btn-default'
            ]) !!}
        </div>

    {!! Form::close() !!}

@endsection
