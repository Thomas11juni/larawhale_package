@extends('lw::layout.default')

@section('content')

    <h1>List: #{{ $list->id }}</h1>

    <h2>List items</h2>

    <?php

        $model = new LaraWhale\App\Models\ListItem();

        $columns = $model->get_columns();

    ?>

    <div class="clearfix">

        <a class="pull-right btn btn-default" href="{{ route('list_items.create', $list->id) }}">

            Create

        </a>

    </div>

    {!! view('lw::module.table', [
        'columns' => $columns,
        'items' => $list->list_items,
        'resource' => 'list_items'
    ])->render() !!}

@endsection
