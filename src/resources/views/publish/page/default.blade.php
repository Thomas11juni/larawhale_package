@extends('layout.default')

@section('main_content')

    @lwSection('header')

        <div class="container-fluid" style="background: url(@lwField('background', 'file', 'header'))">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 text-center">

                        <h1>

                            @lwField('title', 'text', 'header')

                        </h1>

                        <h4>
 
                            @lwField('subtitle', 'text', 'header')

                        </h4>

                    </div>

                </div>

            </div>

        </div>

    @lwEndSection

    @lwList('default')

@endsection
