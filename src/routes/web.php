<?php

use LaraWhale\App\Models\Route as LwRoute;


# Cms routes
# -------------------------------------------------- -->


Route::group([

	'namespace' => 'LaraWhale\App\Http\Controllers\Cms',
	'prefix' => 'cms',
	'middleware' => [
		'web'
	]

], function () {

	Route::get('login', [
		'as' => 'lw_get_login',
		'uses' => 'AuthController@get_login'
	]);

	Route::post('login', [
		'as' => 'lw_post_login',
		'uses' => 'AuthController@post_login'
	]);

	Route::get('logout', [
		'as' => 'lw_get_logout',
		'uses' => 'AuthController@get_logout'
	]);


	Route::group([

		'middleware' => [
			'LaraWhale\App\Http\Middleware\AuthMiddleware'
		]

	], function () {

		Route::get('/', [
			'as' => 'lw_get_index',
			'uses' => 'HomeController@index'
		]);


		Route::resource('pages', 'PageController');


		Route::resource('lists', 'ListController');


		Route::resource('list_items', 'ListItemController');

		Route::get('lists/{id}/list_items/create', [
			'as' => 'list_items.create',
			'uses' => 'ListItemController@create'
		]);

		Route::get('lists/{list_id}/list_items/{list_item_id}/edit', [
			'as' => 'list_items.edit',
			'uses' => 'ListItemController@edit'
		]);

		Route::put('lists/{list_id}/list_items/{list_item_id}/update', [
			'as' => 'list_items.update',
			'uses' => 'ListItemController@update'
		]);

		Route::delete('lists/{list_id}/list_items/{list_item_id}/delete', [
			'as' => 'list_items.delete',
			'uses' => 'ListItemController@destroy'
		]);

	});

});


# Frontend routes
# -------------------------------------------------- -->


Route::group([

	'namespace' => 'LaraWhale\App\Http\Controllers'

], function ()
{

	if(Schema::hasTable('lw_routes'))
	{
		
		$routes = LwRoute::all();

		foreach ($routes as $route)
		{

			Route::get($route->uri, [
				'as' => $route->name,
				'uses' => 'PageController@index'
			]);

		}

	}

});


# -------------------------------------------------- -->
