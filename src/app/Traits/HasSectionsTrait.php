<?php

namespace LaraWhale\App\Traits;


use LaraWhale\App\Models\File;


trait HasSectionsTrait
{

    # processSections
    # -------------------------------------------------- -->

    public function processSections ($request)
    {
        # Delete old sections and cascade fields
        #
        $this->sections()->delete();
        

        # Create new sections and fields
        #
        $sections = $request->input('sections');

        foreach ($sections as $section_key => $section_fields)
        {
            # Create new section
            #
            $section = $this->sections()->create([
                'key' => $section_key
            ]);


            # Loop fields
            #
            foreach ($section_fields as $field_key => $field)
            { 
                # Prepare data
                #
                $data = [
                    'key' => $field['key'],
                    'value' => isset($field['value']) ? $field['value'] : NULL
                ];


                # Switch between field types
                #
                switch ($field['type'])
                {
                    case 'text':
                        $section->text_fields()->create($data);
                    break;

                    case 'textarea':
                        $section->textarea_fields()->create($data);
                    break;

                    case 'file':
                        $files = $request->file('sections');

                        if ($files && $files[$section_key] && $files[$section_key][$field_key])
                        {
                            $file = $files[$section_key][$field_key]['value'];

                            $file = File::upload($file);

                            $section->file_fields()->create([
                                'key' => $field_key,
                                'file_id' => $file->id
                            ]);
                        }
                    break;
                }
            }
        }
    }


    # -------------------------------------------------- -->

}
