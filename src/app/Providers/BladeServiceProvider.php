<?php

namespace LaraWhale\App\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;


use LaraWhale\App\Models\LwList;


class BladeServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        # lwSection blade directive
        #
        Blade::directive('lwSection', function ($expression) {

            return '';

        });


        # lwEndSection blade directive
        #
        Blade::directive('lwEndSection', function () {

            return '';

        });

    }










        /*

        # lwSection blade directive
        #
        Blade::directive('lwSection', function ($expression) {

            return '';

        });


        # lwEndSection blade directive
        #
        Blade::directive('lwEndSection', function () {

            return '';

        });


        # lwField blade directive
        #
        Blade::directive('lwField', function ($expression) {


            $parameters = explode(',', $expression);


            $field_key = $parameters[0];

            $field_type = $parameters[1];

            $section_key = $parameters[2];
            

            $data_string = '[
                "field_key" => ' . $field_key . ',
                "field_type" => ' . $field_type . ',
                "section_key" => ' . $section_key . ',
                "sections" => get_defined_vars()["__data"]["sections"]
            ]';


            $view = '<?php echo $__env->make("lw::module.field.field", ' . $data_string . ')->render(); ?>';

            return $view;

        });


        # lwField blade directive
        #
        Blade::directive('lwList', function ($expression) {

            $parameters = explode(',', $expression);


            $list_name = $parameters[0];

            $list_name = preg_replace('/\'/', '', $list_name);

            $list = LwList::where('title', $list_name)->first();

            if (!$list) return 'List not found';


            //View::composer($list->view, function ($view) use ($list) {

                $view->with('list', $list);

            });


            View::composer($list->list_item_view, function ($view) use ($list) {

                $view->with('list', $list);


                $sections = [];

                $list_item = $list->list_items()->first();

                if ($list_item)
                {
                    foreach ($list_item->sections as $section)
                    {
                        $sections[$section->key] = $section->value;
                    }
                }

                $view->with('sections', $section);

            }); //

            return view($list->view, ['list' => $list])->render();


            return '<?php echo $__env->make("' . $list->view . '", get_defined_vars()); ?>';

        });


    } */


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
    
}
