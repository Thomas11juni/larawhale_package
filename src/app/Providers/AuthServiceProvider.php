<?php

namespace LaraWhale\App\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;


class AuthServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        # Custom auth guard
        #
        $auth_config = $this->app->config['auth'];

        $guards = $auth_config['guards'];

        $providers = $auth_config['providers'];


        $guards['lw'] = $lw_guard = [
            'driver' => 'session',
            'provider' => 'lw_admins'
        ];

        $providers['lw_admins'] = $lw_provider = [
            'driver' => 'eloquent',
            'model' => 'LaraWhale\\App\\Models\\Admin'
        ];


        $auth_config['guards'] = $guards;

        $auth_config['providers'] = $providers;

        $this->app->config['auth'] = $auth_config;

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
    
}







       