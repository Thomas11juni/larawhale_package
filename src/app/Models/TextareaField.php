<?php

namespace LaraWhale\App\Models;


class TextareaField extends Model
{

	# Variables
	# -------------------------------------------------- -->


    protected $fillable = ['section_id', 'key', 'value'];

    protected $table = 'lw_textarea_fields';


	# -------------------------------------------------- -->

}
