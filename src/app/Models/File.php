<?php

namespace LaraWhale\App\Models;


class File extends Model
{

	# Variables
	# -------------------------------------------------- -->


    protected $fillable = ['name', 'filename'];

    protected $table = 'lw_files';

    protected $upload_path = 'uploads';


	# Attributes
	# -------------------------------------------------- -->


	public function getFullUrlAttribute ()
	{

		return asset('uploads/'.$this->filename);

	}


	# Static functions
	# -------------------------------------------------- -->


	static function upload ($file)
	{

		# Create empty instance
		#
		$self = new static;


		# Create hash name
		#
		$name = '';

		$exists = true;

		while ($exists)
		{

			$name = str_random(10);

			$exists = static::where('name', $name)->first();

		}


		# Create filename
		#
		$extension = $file->extension();

		$filename = $name.'.'.$extension;


		# Move file
		#
		$file->move(public_path($self->upload_path), $filename);


		# Create new file
		#
		$file = static::create([
			'name' => $name,
			'filename' => $filename
		]);


		return $file;

	}


	# -------------------------------------------------- -->

}
