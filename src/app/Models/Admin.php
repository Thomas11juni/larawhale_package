<?php

namespace LaraWhale\App\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;


class Admin extends Authenticatable
{

	# Variables
	# -------------------------------------------------- -->


	protected $fillable = ['username', 'password'];

	protected $hidden = ['password', 'remember_token'];

    protected $table = 'lw_admins';



	# -------------------------------------------------- -->

}
