<?php

namespace LaraWhale\App\Models;


class Route extends Model
{

	# Variables
	# -------------------------------------------------- -->


    protected $fillable = ['uri'];

    protected $table = 'lw_routes';


	# Relationships
	# -------------------------------------------------- -->


    public function page ()
    {

    	return $this->belongsTo($this->n_s.'\\Page');

    }


    # Static functions
    # -------------------------------------------------- -->


    public static function rules ($route = NULL)
    {

        $rules = [
            'uri' => 'sometimes|string|unique:lw_routes,uri'
        ];


        # When updating
        #
        if ($route)
        {

            $rules['uri'] .= ','.$route->id;

        }


        return $rules;

    }


	# -------------------------------------------------- -->

}
