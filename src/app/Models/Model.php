<?php

namespace LaraWhale\App\Models;


use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class Model extends Eloquent
{

	# Variables
	# -------------------------------------------------- -->

	public $n_s = 'LaraWhale\App\Models';

	# Supportive functions
	# -------------------------------------------------- -->

    public function get_columns ()
    {
        $columns = [];

        $table = '';


        if (!isset($this->table)) $table = $this->getTable();

        else $table = $this->table;


        $columns = Schema::getColumnListing($this->table);


        return $columns;
    }

	# -------------------------------------------------- -->

}
