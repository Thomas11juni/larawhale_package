<?php

namespace LaraWhale\App\Models;


class Section extends Model
{

	# Variables
	# -------------------------------------------------- -->

    protected $field_relationships = [
        'file_fields',
        'text_fields',
        'textarea_fields'
    ];

    protected $fillable = ['page_id', 'key', 'content'];

    protected $table = 'lw_sections';

    # Attributes
    # -------------------------------------------------- -->

    public function getFieldsAttribute ()
    {
        $fields = collect();


        # Get field relationships and loop
        #
        foreach ($this->field_relationships as $field_relationship)
        {
            $fields = $fields->merge($this->$field_relationship);
        }

        $fields = $fields->keyBy('key');


        return $fields;
    }

    # Relationships
    # -------------------------------------------------- -->

    public function file_fields ()
    {
        return $this->hasMany($this->n_s.'\\FileField');
    }

    public function page ()
    {
    	return $this->belongsTo($this->n_s.'\\Page');
    }

    public function text_fields ()
    {
        return $this->hasMany($this->n_s.'\\TextField');
    }

    public function textarea_fields ()
    {
        return $this->hasMany($this->n_s.'\\TextareaField');
    }

	# -------------------------------------------------- -->

}
