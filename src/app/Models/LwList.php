<?php

namespace LaraWhale\App\Models;


class LwList extends Model
{
    
	# Variables
	# -------------------------------------------------- -->

    protected $fillable = ['title', 'model'];

    protected $table = 'lw_lists';


    # Relationships
    # -------------------------------------------------- -->

    public function list_items ()
    {
        return $this->hasMany($this->model);
    }

    # Static functions
    # -------------------------------------------------- -->

    public static function rules ($list = NULL)
    {
        $rules = [
            'title' => 'required|string|unique:lw_pages,title',
            'model' => 'required|string',
        ];


        # When updating
        #
        if ($list)
        {
            $rules['title'] .= ','.$list->id;
        }


        return $rules;
    }

	# -------------------------------------------------- -->

}
