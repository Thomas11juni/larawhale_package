<?php

namespace LaraWhale\App\Models;


use LaraWhale\App\Traits\HasSectionsTrait;


class ListItem extends Model
{

    # Traits
    # -------------------------------------------------- -->


    use HasSectionsTrait;


	# Variables
	# -------------------------------------------------- -->


    protected $fillable = ['lw_list_id'];

    protected $table = 'lw_list_items';


    # Relationships
    # -------------------------------------------------- -->


    public function list ()
    {

        return $this->belongsTo($this->n_s.'\\LwList', 'lw_list_id', 'id');

    }

    public function sections ()
    {

        return $this->belongsToMany($this->n_s.'\\section', 'lw_list_items_lw_sections');

    }
    

    # Attributes
    # -------------------------------------------------- -->


    public function getViewAttribute ()
    {

        return $this->list->list_item_view;

    }


    # Static functions
    # -------------------------------------------------- -->


    public static function rules ($list_item = NULL)
    {

        $rules = [
            'lw_list_id' => 'required|exists:lw_lists,id'
        ];


        # When updating
        #
        if ($list_item)
        {

        }


        return $rules;

    }


	# -------------------------------------------------- -->

}
