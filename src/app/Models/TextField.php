<?php

namespace LaraWhale\App\Models;


class TextField extends Model
{

	# Variables
	# -------------------------------------------------- -->


    protected $fillable = ['section_id', 'key', 'value'];

    protected $table = 'lw_text_fields';


	# -------------------------------------------------- -->

}
