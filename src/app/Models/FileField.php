<?php

namespace LaraWhale\App\Models;


class FileField extends Model
{

	# Variables
	# -------------------------------------------------- -->

    protected $fillable = ['section_id', 'key', 'file_id'];

    protected $table = 'lw_file_fields';

	# Relationships
	# -------------------------------------------------- -->

    public function file ()
    {
        return $this->belongsTo($this->n_s.'\\File');
    }

	# -------------------------------------------------- -->

}
