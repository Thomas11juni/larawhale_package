<?php

namespace LaraWhale\App\Models;


class Image extends File
{

	# Variables
	# -------------------------------------------------- -->


    protected $fillable = ['file_id'];

    protected $table = 'lw_images';


	# Relationships
	# -------------------------------------------------- -->


    public function file ()
    {

    	return $this->belongsTo($this->n_s.'\\File');

    }


	# -------------------------------------------------- -->

}
