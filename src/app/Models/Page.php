<?php

namespace LaraWhale\App\Models;


use File;
use LaraWhale\App\Traits\HasSectionsTrait;


class Page extends Model
{

    # Traits
    # -------------------------------------------------- -->

    use HasSectionsTrait;

	# Variables
	# -------------------------------------------------- -->

    protected $fillable = ['title', 'view', 'content'];

    protected $table = 'lw_pages';

	# Relationships
	# -------------------------------------------------- -->

    public function route ()
    {
        return $this->hasOne($this->n_s.'\\Route');
    }

    public function sections ()
    {
        return $this->hasMany($this->n_s.'\\Section');
    }

    # Static functions
    # -------------------------------------------------- -->

    public static function rules ($page = NULL)
    {
        $rules = [
            'title' => 'required|string|unique:lw_pages,title',
            'view' => 'required|string',
            'content' => 'string'
        ];


        # When updating
        #
        if ($page)
        {
            $rules['title'] .= ','.$page->id;
        }


        return $rules;
    }

	# -------------------------------------------------- -->

}
