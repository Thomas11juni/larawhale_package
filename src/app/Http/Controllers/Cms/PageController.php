<?php

namespace LaraWhale\App\Http\Controllers\Cms;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use LaraWhale\App\Models\File;
use LaraWhale\App\Models\Page;
use LaraWhale\App\Models\Route;
use LaraWhale\App\Models\Section;


class PageController extends Controller
{

    public function index()
    {
        $data = [
            'pages' => Page::all()
        ];

        return view('lw::page.index', $data);
    }


    public function create()
    {
        return view('lw::page.form');
    }


    public function store(Request $request)
    {
        # Merge rules
        #
        $rules = array_merge(Route::rules(), Page::rules());


        # Validate
        #
        $this->validate($request, $rules);


        # Create page and route
        #
        $page = Page::create($request->input());

        $route = $page->route()->create([
            'uri' => $request->uri
        ]);


        return redirect(route('pages.index'));
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        # Check if page exists
        #
        $page = Page::find($id);

        if (!$page) abort(404);


        $data = [
            'page' => $page
        ];

        return view('lw::page.form', $data);
    }


    public function update(Request $request, $id)
    {
        # Check if page exists
        #
        $page = Page::find($id);

        if (!$page) abort(404);


        $route = $page->route;


        # Merge rules
        #
        $rules = array_merge(Route::rules($route), Page::rules($page));


        # Validate
        #
        $this->validate($request, $rules);


        # Fill page and save
        #
        $page->fill($request->input());

        $page->save();


        # Process sections
        #
        $page->processSections($request);


        return redirect(route('pages.index'));
    }


    public function destroy($id)
    {
        # Check if page exists
        #
        $page = Page::find($id);

        if (!$page) abort(404);


        # Delete page
        #
        $page->delete();


        return redirect()->back();
    }

}
