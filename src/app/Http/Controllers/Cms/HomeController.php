<?php

namespace LaraWhale\App\Http\Controllers\Cms;


use App\Http\Controllers\Controller;

use LaraWhale\App\Models\Admin;


class HomeController extends Controller
{

	public function index()
	{

		return view('lw::cms.index');

	}

}
