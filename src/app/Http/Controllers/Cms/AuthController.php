<?php

namespace LaraWhale\App\Http\Controllers\Cms;


use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{

	protected $redirectTo = '/cms';


	public function get_login ()
	{

		return view('lw::auth.login');

	}


	public function post_login (Request $request)
	{

		$data = [
			'username' => $request->input('username'),
			'password' => $request->input('password')
		];

		if (!Auth::guard('lw')->attempt($data))
		{
			return redirect()->back();
		}


		return redirect()->route('lw_get_index');

	}


	public function get_logout ()
	{

		Auth::guard('lw')->logout();


		return redirect()->route('lw_get_login');

	}

}