<?php

namespace LaraWhale\App\Http\Controllers\Cms;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use LaraWhale\App\Models\Route;


class RouteController extends Controller
{

    public function index()
    {

        $data = [
            'routes' => Route::all()
        ];


        return view('lw::route.index', $data);

    }


    public function create()
    {

        return view('lw::page.form');

    }


    public function store(Request $request)
    {

        # Merge rules
        #
        $rules = array_merge(Route::rules());


        # Validate
        #
        $this->validate($request, $rules);


        # Create page and route
        #
        $page = Route::create($request->input());


        return redirect(route('routes.index'));

    }


    public function show($id)
    {

    }


    public function edit($id)
    {

        # Check if page exists
        #
        $page = Page::find($id);

        if (!$page) abort(404);


        $data = [
            'page' => $page
        ];

        return view('lw::page.form', $data);

    }


    public function update(Request $request, $id)
    {

        # Check if page exists
        #
        $page = Page::find($id);

        if (!$page) abort(404);


        $route = $page->route;


        # Merge rules
        #
        $rules = array_merge(Route::rules($route), Page::rules($page));


        # Validate
        #
        $this->validate($request, $rules);


        # Fill page and save
        #
        $page->fill($request->input());

        $page->save();


        # Delete old sections
        #
        $page->sections()->delete();
        

        # Create new sections
        #
        $section_keys = $request->input('section_keys');

        $section_contents = $request->input('section_contents');


        if ($section_keys)
        {

            foreach ($section_keys as $i => $section_key)
            {

                if (!array_key_exists($i, $section_contents)) continue;


                $section = Section::create([
                    'page_id' => $page->id,
                    'key' => $section_key,
                    'content' => $section_contents[$i]
                ]);

                $section->save();

            }

        }


        return redirect(route('pages.index'));
    }


    public function destroy($id)
    {

        # Check if page exists
        #
        $page = Page::find($id);

        if (!$page) abort(404);


        # Delete page
        #
        $page->delete();


        return redirect()->back();

    }

}
