<?php

namespace LaraWhale\App\Http\Controllers\Cms;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use LaraWhale\App\Models\File;
use LaraWhale\App\Models\LwList;


class ListItemController extends Controller
{

    public function index()
    {

    }


    public function create($list_id)
    {
        # Check if list exists
        #
        $list = LwList::find($list_id);

        if (!$list) abort(404);


        $data = [
            'list' => $list,
            'list_item' => new $list->model()
        ];


        return view('lw::list_item.form', $data);
    }


    public function store(Request $request)
    {
        # Check if list exists
        #
        $list = LwList::find($request->lw_list_id);

        if (!$list) abort(404);

        $list_item_model = $list->model;


        # Get rules
        #
        $rules = [];

        if (method_exists($list_item_model, 'rules')) $rules = $list_item_model::rules();


        # Validate
        #
        $this->validate($request, $rules);


        # Create list item
        #
        $list_item = $list->list_items()->create($request->input());


        # Handle files if there are
        #
        foreach ($list_item->fields as $key => $type)
        {
            if ($type != 'file') continue;

            $file = $request->file($key);

            if (!$file) continue;

            $file = File::upload($file);


            $list_item->{$key . '_file_id'} = $file->id;

            $list_item->save();
        }


        return redirect(route('lists.show', $request->lw_list_id));
    }


    public function show($id)
    {

    }


    public function edit($list_id, $list_item_id)
    {
        # Check if list and list item exists
        #
        $list = LwList::find($list_id);

        if (!$list) abort(404);

        $model = $list->model;
        
        $list_item = $model::find($list_item_id);

        if (!$list_item) abort(404);


        $data = [
            'list' => $list,
            'list_item' => $list_item
        ];


        return view('lw::list_item.form', $data);
    }


    public function update(Request $request, $list_id, $list_item_id)
    {
        # Check if list and list item exists
        #
        $list = LwList::find($list_id);

        if (!$list) abort(404);

        $model = $list->model;
        
        $list_item = $model::find($list_item_id);

        if (!$list_item) abort(404);


        # Get rules
        #
        $rules = [];

        if (method_exists($list_item, 'rules')) $rules = $list_item::rules();


        # Validate
        #
        $this->validate($request, $rules);


        # Fill list item and save
        #
        $list_item->fill($request->input());

        $list_item->save();


        # Handle files if there are
        #
        foreach ($list_item->fields as $key => $type)
        {
            if ($type != 'file') continue;

            $file = $request->file($key);

            if (!$file) continue;

            $file = File::upload($file);


            $list_item->{$key . '_file_id'} = $file->id;

            $list_item->save();
        }


        return redirect(route('lists.show', $request->lw_list_id));
    }


    public function destroy($list_id, $list_item_id)
    {
        # Check if list and list item exists
        #
        $list = LwList::find($list_id);

        if (!$list) abort(404);

        $model = $list->model;
        
        $list_item = $model::find($list_item_id);

        if (!$list_item) abort(404);


        # Delete list item
        #
        $list_item->delete();


        return redirect()->back();
    }

}
