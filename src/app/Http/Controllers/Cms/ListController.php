<?php

namespace LaraWhale\App\Http\Controllers\Cms;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use LaraWhale\App\Models\LwList;


class ListController extends Controller
{

    public function index()
    {
        $data = [
            'lists' => LwList::all()
        ];

        return view('lw::list.index', $data);
    }


    public function create()
    {
        return view('lw::list.form');
    }


    public function store(Request $request)
    {
        # Merge rules
        #
        // $rules = array_merge(Route::rules(), List::rules());

        $rules = LwList::rules();


        # Validate
        #
        $this->validate($request, $rules);


        # Create list and route
        #
        $list = LwList::create($request->input());

        // $route = $list->route()->create([
        //     'uri' => $request->uri
        // ]);


        return redirect(route('lists.index'));
    }


    public function show($id)
    {
        # Check if list exists
        #
        $list = LwList::find($id);

        if (!$list) abort(404);


        $data = [
            'list' => $list
        ];

        return view('lw::list.show', $data);
    }


    public function edit($id)
    {
        # Check if list exists
        #
        $list = LwList::find($id);

        if (!$list) abort(404);


        $data = [
            'list' => $list
        ];

        return view('lw::list.form', $data);
    }


    public function update(Request $request, $id)
    {
        # Check if list exists
        #
        $list = LwList::find($id);

        if (!$list) abort(404);


        $route = $list->route;


        # Merge rules
        #
        // $rules = array_merge(Route::rules($route), LwList::rules($list));

        $rules = LwList::rules();


        # Validate
        #
        $this->validate($request, $rules);


        # Fill list and save
        #
        $list->fill($request->input());

        $list->save();


        return redirect(route('lists.index'));
    }


    public function destroy($id)
    {
        # Check if list exists
        #
        $list = LwList::find($id);

        if (!$list) abort(404);


        # Delete list
        #
        $list->delete();


        return redirect()->back();
    }

}
