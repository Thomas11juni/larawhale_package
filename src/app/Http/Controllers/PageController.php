<?php

namespace LaraWhale\App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

use LaraWhale\App\Models\Page;
use LaraWhale\App\Models\Route;


class PageController extends Controller
{
	public function index (Request $request)
	{
		# Get uri
		#
		$uri = $request->route()->getUri();

		$uri = ($uri != '/' ? $uri : '');


		# Check route exists
		#
		$route = Route::where('uri', $uri)->first();

		if (!$route) abort(404);


		# Check page exists
		#
		$page = $route->page;

		if (!$page) abort(404);


		# Get page sections
		#
		$sections = [];

		foreach ($page->sections as $section)
		{
			$sections[$section->key] = $section->fields;
		}


		# Share page and sections with field view
		#
		View::composer('lw::module.field.field', function ($view) use ($page, $sections) {

			$view->with('page', $page);

			$view->with('sections', $sections);

		});


		return view($page->view, ['page' => $page]);
	}

}
