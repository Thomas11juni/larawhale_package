<?php

namespace LaraWhale\App\Http\Middleware;


use Auth;
use Closure;


class AuthMiddleware
{

	public function handle($request, Closure $next)
	{

		if (!Auth::guard('lw')->check())
		{
			return redirect(route('lw_get_login'));
		}

		return $next($request);

	}

}