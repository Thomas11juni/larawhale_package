<?php

namespace LaraWhale\App\Http\ViewComposers;


use Illuminate\View\View;
use App\Repositories\UserRepository;

use LaraWhale\App\Models\Page;


class NavComposer
{

    public function __construct()
    {

    }


    public function compose(View $view)
    {

        # Get pages and share
        #
        $pages = Page::all();

        $view->with('pages', $pages);

    }

}
