<?php

namespace LaraWhale\App\Console\Commands;


use Artisan;
use Illuminate\Console\Command;


class CmsMigrateRefreshCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lw:migrate:refresh {seed?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes database';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Artisan::call('migrate:refresh');


        $seed = $this->argument('seed');

        if ($seed && $seed == 'seed')
        {

            Artisan::call('lw:db:seed');

        }

    }

}
