<?php

namespace LaraWhale\App\Console\Commands;


use Artisan;
use Illuminate\Console\Command;


class CmsDbSeedCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lw:db:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds database';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Artisan::call('db:seed', [
            '--class' => 'LaraWhale\\Database\\Seeds\\DatabaseSeeder'
        ]);

    }

}
