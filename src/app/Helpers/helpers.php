<?php

use Illuminate\Support\Facades\Schema;
use LaraWhale\App\Helpers\ViewHelper;
use LaraWhale\App\Models\LwList;


if (!function_exists('get_columns'))
{
    function get_columns ($model)
    {
        $table = $model->getTable();
        

        $columns = [];

        $columns = Schema::getColumnListing($table);


        return $columns;
    }
}


if (!function_exists('lwField'))
{
    function lwField ($field_key, $field_type, $section_key)
    {
        return ViewHelper::lwField($field_key, $field_type, $section_key);
    }
}


if (!function_exists('lwList'))
{
    function lwList ($title)
    {
        return LwList::where('title', $title)->first();
    }
}
