<?php

namespace LaraWhale\App\Helpers;


use File;


class ViewHelper
{

    # get_view_sections
    # -------------------------------------------------- -->

    public static function get_view_sections ($view)
    {        
        # Create file name and create path
        #
        $file_name = preg_replace('/\./', '/', $view).'.blade.php';


        $path = resource_path('views/'.$file_name);


        # Get file content
        #
        $file = File::get($path);


        # Get fields
        #
        $field_matches = [];

        preg_match_all('/(?<=lwField\().*?(?=\))/', $file, $field_matches);

        
        # Loop all matches and create sections structure
        #
        $sections = [];

        foreach ($field_matches[0] as $field_match)
        {
            # Remove unwanted, explode and declare parameters
            #
            $field_match = preg_replace('/\'/', '', $field_match);

            $field_match = preg_replace('/\s*\,\s*/', ',', $field_match);
            
            $parameters = explode(',', $field_match);

            $field_key = $parameters[0];

            $field_type = $parameters[1];

            $section_key = $parameters[2];


            # Create section if not in array
            #
            if (!array_key_exists($section_key, $sections))
            {
                $section = [
                    'key' => $section_key,
                    'fields' => []
                ];
            }
            else
            {
                $section = $sections[$section_key];
            }


            # Create field and insert into section
            #
            $field = [
                'key' => $field_key,
                'type' => $field_type
            ];

            $section['fields'][] = $field;


            # Store section back in array
            #
            $sections[$section_key] = $section;
        }


        return $sections;
    }


    # lw_field
    # -------------------------------------------------- -->

    public static function lwField ($key, $type, $section)
    {
        $data = [
            'lwField' => [
                'key' => $key,
                'type' => $type,
                'section' => $section
            ]
        ];

        
        return view('lw::module.field.field', $data)->render();
    }

    # -------------------------------------------------- -->

}
