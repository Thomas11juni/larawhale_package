<?php

namespace LaraWhale;


use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;


class CmsServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        # Load migrations
        #
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');


        # Load commands
        #
        $n_s = 'LaraWhale\\App\\Console\\Commands';

        if ($this->app->runningInConsole())
        {

            $this->commands([
                $n_s.'\\CmsMigrateRefreshCommand',
                $n_s.'\\CmsDbSeedCommand'
            ]);

        }
        

        # Load routes
        #
        if (!$this->app->routesAreCached())
        {

            require __DIR__ . '/routes/web.php';

        }


        # Load views
        #
        $this->loadViewsFrom(__DIR__.'/resources/views', 'lw');


        # Publishables
        #
        $this->publishes([
            __DIR__.'/resources/views/publish' => resource_path('views')
        ], 'lw.views');


        # View composers
        #
        $n_s = 'LaraWhale\\App\\Http\\ViewComposers';

        View::composer('*', $n_s.'\\NavComposer');


    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        # Register service providers
        #
        $service_providers = [
            'Illuminate\Html\HtmlServiceProvider',
            'LaraWhale\App\Providers\AuthServiceProvider',
            'LaraWhale\App\Providers\BladeServiceProvider'
        ];

        foreach ($service_providers as $service_provider)
        {
            $this->app->register($service_provider);
        }


        # Register aliases
        #
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        
        $loader->alias('Form', 'Illuminate\Html\FormFacade');

        $loader->alias('Html', 'Illuminate\Html\HtmlFacade');

        $loader->alias('ViewHelper', 'LaraWhale\App\Helpers\ViewHelper');

    }
    
}
