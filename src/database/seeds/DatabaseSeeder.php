<?php

namespace LaraWhale\Database\Seeds;


use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $n_s = 'LaraWhale\Database\Seeds';


        $seeders = [
            'AdminsTableSeeder',
            'PagesTableSeeder',
            'ListsTableSeeder'
        ];

        foreach ($seeders as $seeder)
        {

            $class = $n_s.'\\'.$seeder;

            $this->call($class);

        }

    }

}
