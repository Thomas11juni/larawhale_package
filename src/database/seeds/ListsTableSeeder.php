<?php

namespace LaraWhale\Database\Seeds;


use Illuminate\Database\Seeder;

use LaraWhale\App\Models\LwList;


class ListsTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $list = LwList::firstOrCreate([
            'title' => 'default',
            'model' => 'list.default'
        ]);

    }

}
