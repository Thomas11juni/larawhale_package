<?php

namespace LaraWhale\Database\Seeds;


use Illuminate\Database\Seeder;

use LaraWhale\App\Models\Page;


class PagesTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $home = Page::firstOrCreate([
            'title' => 'home',
            'view' => 'page.default'
        ]);

        $home->route()->create([
            'uri' => ''
        ]);

    }

}
