<?php

namespace LaraWhale\Database\Seeds;


use Hash;
use Illuminate\Database\Seeder;

use LaraWhale\App\Models\Admin;


class AdminsTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = Admin::firstOrCreate([
            'username' => 'admin',
            'password' => Hash::make('admin')
        ]);

    }

}
